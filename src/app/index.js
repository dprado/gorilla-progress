import './app.css';
import './progress.css';

document.addEventListener('DOMContentLoaded', () => {
    const TOTAL = 125;
    let progress = 56;
    let percentage = 0;
    let $progressBar = document.querySelector('.progress-bar .inner-progress');
    let $progressLabel = $progressBar.querySelector('.progress-label');
    let $progressTargetLabel = document.querySelector('.progress-target-label');
    let $neededProgress = document.querySelector('.needed-progress');

    let calculatePercentage = () => parseFloat((100 - 100 * ((TOTAL - progress) / TOTAL)).toFixed(2));
    
    let updateProgress = () => {
        $progressLabel.innerHTML = `<i class="fa fa-chevron-up"></i> $${progress}`;
        $progressBar.style.width = `${percentage}%`
        $progressTargetLabel.innerHTML = `Target <span>$${TOTAL}</span>`;
        $neededProgress.innerHTML = ` <i class="fa fa-info-circle"></i> You need $${TOTAL - progress} more to reach your target.`
    };
    updateProgress();
    percentage = calculatePercentage();
    setTimeout(updateProgress, 750);
});
