var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

exports.context = path.join(__dirname, 'src');

exports.devtool = 'source-map';

exports.entry = {
    'app': [ 'babel-polyfill', './app/index.js' ]
};

exports.output = {
    path: './public/',
    filename: '[name].bundle.js'
};

exports.module = {
    loaders: [
        { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
        { test: /\.css$/, loader: ExtractTextPlugin.extract('css-loader'), exclude: /node_modules/ },
        { test: /\.html$/, loader: 'html', exclude: /node_modules/ },
        { test: /\.json$/, loader: 'file?name=[name].[ext]', exclude: /node_modules/ },
        { test: /\.jpeg/, loader: 'file?name=[name].[ext]', exclude: /node_modules/ }
    ]
};

exports.plugins = [
    new ExtractTextPlugin("styles.css"),
    new CopyWebpackPlugin([
        { from: 'CNAME' }
    ]),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
        inject: true,
        hash: true,
        template: 'index.html',
        filename: 'index.html'
    })
];

